#include <Arduino.h>

/*************************************************************************************
 *HIGH LOW oder LOW HIGH für Forward?  
 *Spontanitäten: Motoren ungleich schwach? Karosserie schleift?
 *  ->Test
*************************************************************************************/
#define WAIT_TO_START 10000

///Sensor
#define SENSOR_L A0
#define SENSOR_R A1

///Motor

//left
#define LEFT_ENABLE_1 9           //digital out, both need to be HIGH in order for the motors to funktion
#define RIGHT_ENABLE_1 8          //digital out
#define RPWM_MOTOR_1 10           //analog out (PWM + direktion)
#define LPWM_MOTOR_1 11           //analog out (PWM + direktion)

//right
#define LEFT_ENABLE_2 4           //digital out, both need to be HIGH in order for the motors to funktion
#define RIGHT_ENABLE_2 3          //digital out
#define RPWM_MOTOR_2 5            //analog out (PWM + direktion)
#define LPWM_MOTOR_2 6            //analog out (PWM + direktion)

/**********************************************************************
  funktions
**********************************************************************/

/**
 * Method for sensoring the line
 * Gives back wether the surface reflects or not
 */

bool lineLeft()
{
  return !digitalRead(SENSOR_L);
}

bool lineRight()
{
  return !digitalRead(SENSOR_R);
}

 /**
 * Method for driving
 * When value is negative drive backwards, when positive forwards, when equal to zero stop engines
 * 
 * @param speed Speed to drive with
 */

 void drive(int speed)
{
  if (speed > 0) 
  {
    digitalWrite(LPWM_MOTOR_1, speed);
    digitalWrite(RPWM_MOTOR_1, LOW);

    digitalWrite(LPWM_MOTOR_2, LOW);
    digitalWrite(RPWM_MOTOR_2, speed);
  }
  else if (speed == 0)
  {
    digitalWrite(LPWM_MOTOR_1, LOW);
    digitalWrite(RPWM_MOTOR_1, LOW);

    digitalWrite(LPWM_MOTOR_2, LOW);
    digitalWrite(RPWM_MOTOR_2, LOW);
  }
  else if (speed < 0) 
  {
    digitalWrite(LPWM_MOTOR_1, LOW);
    digitalWrite(RPWM_MOTOR_1, speed);

    digitalWrite(LPWM_MOTOR_2, speed);
    digitalWrite(RPWM_MOTOR_2, LOW);
  }
}

/**
 * Method for turning the robot
 * When value is negative turn to the left, when positive turn right
 * 
 * @param speed Speed to turn with
 */

void rotate(int speed) 
{
  if (speed < 0)
  {
    digitalWrite(LPWM_MOTOR_1, LOW);
    digitalWrite(RPWM_MOTOR_1, abs(speed));

    digitalWrite(LPWM_MOTOR_2, LOW);
    digitalWrite(RPWM_MOTOR_2, abs(speed));
  }
  }
  else if (speed > 0)
  {
    digitalWrite(LPWM_MOTOR_1, speed);
    digitalWrite(RPWM_MOTOR_1, LOW);

    digitalWrite(LPWM_MOTOR_2, speed);
    digitalWrite(RPWM_MOTOR_2, LOW);
  }
}

/**********************************************************************
  setup
**********************************************************************/

void setup()
{
  pinMode(SENSOR_L, INPUT);
  pinMode(SENSOR_R, INPUT);
  
  pinMode(LEFT_ENABLE_1, OUTPUT);
  pinMode(RIGHT_ENABLE_1, OUTPUT);
  pinMode(RPWM_MOTOR_1, OUTPUT);
  pinMode(LPWM_MOTOR_1, OUTPUT);

  pinMode(LEFT_ENABLE_2, OUTPUT);
  pinMode(RIGHT_ENABLE_2, OUTPUT);
  pinMode(RPWM_MOTOR_2, OUTPUT);
  pinMode(RPWM_MOTOR_2, OUTPUT);

  drive(0);
  delay(WAIT_TO_START);
  digitalWrite(LEFT_ENABLE_1, HIGH);      //all enables have to be HIGH in order for the motors to work
  digitalWrite(RIGHT_ENABLE_1, HIGH);     //the direction is controlled by wether LPWM or RPWM is used
  digitalWrite(LEFT_ENABLE_2, HIGH);
  digitalWrite(RIGHT_ENABLE_2, HIGH);
  drive(100);
}

/**********************************************************************
  loop
**********************************************************************/

void loop()
{
  if (lineLeft())
  {
    rotate(-50);
    delay(100);
    drive(100);
  }

  if (lineRight())
  {
    rotate(50);
    delay(100);
    drive(100);
  }
}